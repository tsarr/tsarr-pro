import portfolio from '../../data/portfolio/data.json'

// const _products = [
//   {
//     'id': 3,
//     'title': 'Iphone X',
//     'url': 'port-3-link',
//     'desc': 'Далеко-далеко за словесными горами в стране, гласных и согласных живут рыбные тексты.',
//     'img': 'https://www.mockupworld.co/wp-content/uploads/edd/2017/09/free-iphone-x-with-apple-watch-3-mockup-psd-1000x750.jpg'
//   },
//   {
//     'id': 2,
//     'title': 'iPad 4 Mini',
//     'url': 'port-2-link',
//     'desc': 'Далеко-далеко за словесными горами в стране, гласных и согласных живут рыбные тексты.',
//     'img': 'https://www.mockupworld.co/wp-content/uploads/edd/2017/05/free-ipad-display-black-mockup.jpg'
//   },
//   {
//     'id': 1,
//     'title': 'Charli XCX - Sucker CD',
//     'url': 'port-1-link',
//     'desc': 'Далеко-далеко за словесными горами в стране, гласных и согласных живут рыбные тексты.',
//     'img': 'https://www.mockupworld.co/wp-content/uploads/edd/2017/05/free-iphone-display-black-mockup.jpg'
//   }
// ]

export default {
  getProducts (cb) {
    setTimeout(() => cb(portfolio), 100)
  },

  buyProducts (products, cb, errorCb) {
    setTimeout(() => {
      // simulate random checkout failure.
      (Math.random() > 0.5 || navigator.userAgent.indexOf('PhantomJS') > -1)
        ? cb()
        : errorCb()
    }, 100)
  }
}
