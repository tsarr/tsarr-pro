import Vue from 'vue'
import Router from 'vue-router'
import about from '@/pages/about/about'
import contacts from '@/pages/contacts/contacts'
import HomePage from '@/pages/home/HomePage'
import p404 from '@/pages/notFound/notFound'
import services from '@/pages/services/services'
import portfolio from '@/pages/portfolio/portfolio'

Vue.use(Router)

export default new Router({
  mode: 'history',
  fallback: false,
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      name: '1',
      component: HomePage
    },
    {
      path: '/services',
      name: '2',
      component: services
    },
    {
      path: '/about',
      name: '4',
      component: about
    },
    {
      path: '/contacts',
      name: '5',
      component: contacts
    },
    {
      path: '/portfolio/:name',
      name: '3',
      component: portfolio
    },
    {
      path: '/*',
      name: '6',
      component: p404
    }
  ]
})
