const state = {
  lang: false
}

const getters = {
  lang (state) {
    return state.lang
  }
}

export default {
  state,
  getters
}
