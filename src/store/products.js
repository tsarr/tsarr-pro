const state = {
  portfolioItems: [
    {
      code: 'iphone',
      id: 3,
      title: 'Iphone X',
      title_en: 'Iphone X',
      desc: 'Далеко-далеко за словесными горами в стране, гласных и согласных живут рыбные тексты.',
      desc_en: 'Далеко-далеко за словесными горами в стране, гласных и согласных живут рыбные тексты.',
      img: 'https://v.imeg.top/uploads/2019/03/27-1_q_resd.jpg',
      link: 'http://tsarr.pro'
    }
  ],
  portfolioItemsLength: 1
}

const getters = {
  allPortfolio (state) {
    return state.portfolioItems
  }
}

export default {
  state,
  getters
}
