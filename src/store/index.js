import Vue from 'vue'
import Vuex from 'vuex'
import products from './products'
import portfolio from './portfolio'
import languagePack from './languagePack'

import Firebase from 'firebase'
import 'Firebase/firestore'

let config = {
  apiKey: 'AIzaSyDCbGEXf9tiKYdfMhIcoBvuividgA_CLho',
  authDomain: 'tsarr-51c09.firebaseapp.com',
  databaseURL: 'https://tsarr-51c09.firebaseio.com',
  projectId: 'tsarr-51c09',
  storageBucket: 'tsarr-51c09.appspot.com',
  messagingSenderId: '693778898120'
}

Firebase.initializeApp(config)
// let db = app.database()
// let portfolioRef = db.ref('portfolio')

Vue.use(Vuex)

let state = {
  db: Firebase.firestore()
}

const store = new Vuex.Store({
  state,
  modules: {
    products,
    portfolio,
    languagePack
  }
})

export default store
