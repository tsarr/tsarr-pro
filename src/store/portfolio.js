const state = {
  portfolio: [
    {
      id: 3,
      title: 'Iphone X',
      title_en: 'Iphone X',
      desc: 'Далеко-далеко за словесными горами в стране, гласных и согласных живут рыбные тексты.',
      desc_en: 'Далеко-далеко за словесными горами в стране, гласных и согласных живут рыбные тексты.',
      img: 'https://v.imeg.top/uploads/2019/03/52_q_resd.jpg',
      link: 'http://tsarr.pro'
    }
  ]
}

const getters = {
  portfolioList (state) {
    return state.portfolio
  }
}

const mutations = {
  setPortfolio (state, {data}) {
    state.portfolio = data
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  getters
}
