// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/index'
import { sync } from 'vuex-router-sync'
import VueFire from 'vuefire'
Vue.use(VueFire)
import VueResource from 'vue-resource'
import Toasted from 'vue-toasted'

Vue.use(Toasted)
import VueAnalytics from 'vue-analytics'

Vue.use(VueAnalytics, {
  id: 'UA-86512447-1'
})

let optionsError = {
  type: 'error',
  duration: 5000
}

// register the toast with the custom message
Vue.toasted.register('error',
    (payload) => {
      if (!payload.message) {
        return 'Oops.. Something Went Wrong..'
      }
      return payload.message
    },
    optionsError
)

let optionsOk = {
  type: 'success',
  duration: 5000
}

// register the toast with the custom message
Vue.toasted.register('ok',
    (payload) => {
      if (!payload.message) {
        return 'Ok.. all good!'
      }
      return payload.message
    },
    optionsOk
)

Vue.use(VueResource)
sync(store, router)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
  render: h => h(App)
})
